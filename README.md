# de-code
German 105-key layout for programmers

![de-code keyboard layout](de-code.png "de-code keyboard layout")

Features
========


No upper numbers row
--------------------
I have a tenkey block and make heavy use of it for numbers. For even better usability:

- the numbers in the tenkey block become real numbers, as if they were in the upper numbers row. So the Num_Lock key doesn't apply to them. They are numbers! Always!

Germans use a comma as the decimal separator and a period as the thousands separator, just the other way as in the US. But all programming languages I know of are doing it the US way. So the seperator key now gives a period in normal mode, and a comma in shifted mode or - if this hurts you - you can permanently switch to comma with Num_Lock. In that case you will get the period if Shift is pressed.


No AltGr key-combos anymore
---------------------------

There were a number of symbols on German keyboards, that could only be reached via the AltGr-Key. E.g. @, €, |, [, ], ~, \. Especially the brackets hurt, when you use your keyboard for programming. And as the keyboard was designed before the HTML era the symbols < and > aren't even used as angled brackets.

- all bracket like characters now go to the upper numbers row (7 to 0).
That way the < and > chracters are just beside the = chracter to use it in "greater or equal" or "smaller or equal" expressions.

- the backslash symbol moved to a shifted slash key which itself is now reachable in unshifted mode.

- the | char moved to the & char. In some programming languages they are used as logigal "or" & "and".

- the single ' is now the unshifted state of the double ".

- the ? is now the unshifted state of the !, to give place for the =.

- the = moved a key to the right, to give place for the angled brackets.

- the ~ moved a key down, as there is place where the ' once was.
